#include "char_stack.hpp"

CharStack::CharStack(int newMax) {
	max = newMax;
	stack = new char[max];
}
CharStack::~CharStack() {
	delete [] stack;
}
bool CharStack::push(char c) {
	if (size == max) {
		return false;
	}

	stack[size++] = c;

	return true;
}
char CharStack::pop() {
	if (size == 0) {
		return 0;
	}
	return stack[--size];
}
bool CharStack::isEmpty() const {
	return size == 0;
}
