#include <iostream>
#include "char_stack.hpp"
#include "pair_matcher.hpp"

using namespace std;

int main() {
    PairMatcher matcher('(', ')');
    
    string testString = "((())()";
    
    cout << testString << " is " << (matcher.check(testString) ? "valid" : "invalid") << endl;
    return 0;
}
