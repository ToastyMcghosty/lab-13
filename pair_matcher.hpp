#ifndef PAIR_MATCHER_HPP_
#define PAIR_MATCHER_HPP_

using namespace std;

	class PairMatcher {
private:
    char _openChar, _closeChar;
    CharStack charStack;
public:
    PairMatcher(char openChar, char closeChar);
    bool check(const string &testString);


#endif /* PAIR_MATCHER_HPP_ */
