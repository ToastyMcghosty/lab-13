#ifndef CHAR_STACK_HPP_
#define CHAR_STACK_HPP_

class CharStack {
private:
    char *stack;
    int max;
    int size = 0;
public:
    CharStack(int newMax);
    ~CharStack();
    bool push(char c);
    char pop();
    bool isEmpty() const;
};

#endif /* CHAR_STACK_HPP_ */
